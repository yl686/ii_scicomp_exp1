import numpy as np
import math

"""
The cleaned-up version of the source file containing class HuckelSystem.
Also contains required internal function(s)
"""

#internal functions
def _get_evals(matrix):
    """returns the eigenvalues of a matrix."""
    eigvals, eigvecs = np.linalg.eig(matrix)
    return eigvals

#classes
class HuckelSystem:
    """Linear, cyclic and Platonic Huckel pi systems.
    Each instance is a pi system of one of these three types.

    Variables:
        matrix:    ndarray  the Huckel matrix for the system
        evals:     tuple    a set of eigenvalues in float
        type:      str      the type of system
                            'linear' or 'cyclic' or 'Platonic'

    Methods:
        create_polyene_lin(cls, n, a=0, b=-1)
            creates linear Huckel system object of n atoms

        create_polyene_cyc(cls, n, a=0, b=-1)
            creates cyclic Huckel system object of n atoms

        create_platonic(cls, n, a=0, b=-1)
            creates Platonic solid (n vertices) Huckel system object

       degeneracy(self, dp=4)
           prints the list of energies and number of degenerate MOs with
           the energies. dp specifies the abs. tolerance

    """

    def __init__(self, matrix, type):
        """ The instantiation is expected to be done by classmethods
        below, so that the type of system can be appropriately considered.
        """
        self._matrix = matrix
        self._evals = _get_evals(matrix)
        self.type = type

    def __str__(self):
        return str(self.matrix)

    # Handling property dependencies
    @property
    def matrix(self):
        return self._matrix

    @property
    def evals(self):
        return self._evals

    @matrix.setter
    def matrix(self,new_matrix):
        """This special setter is needed so that self.evals can be
        updated automatically when self.matrix is altered.
        """
        self._matrix = new_matrix
        self._evals = _get_evals(new_matrix)

    # classmethods
    @classmethod
    def create_polyene_lin(cls, n, a=0, b=-1):
        """ Creates linear pi system of length n, with a and b specified.
        OUTPUT: an instance of class Huckelsystem with the appropriate
                linear polyene Huckel matrix.
        """
        matrix = np.zeros((n,n))
        for i in range(n):
            matrix[i][i] = a
            if i > 0:
                matrix[i][i-1] = b
            if i < n-1:
                matrix[i][i+1] = b
        return cls(matrix,"linear")

    @classmethod
    def create_polyene_cyc(cls, n, a=0, b=-1):
        """Creates a cyclic polyene object."""
        matrix = np.zeros((n,n))
        for i in range(n):
            matrix[i][i] = a
            matrix[i][i-1] = b
            matrix[i][(i+1)%n] = b
        return cls(matrix,"cyclic")

    @classmethod
    def create_platonic(cls, n, a=0, b=-1):
        """ Generates Huckel matrix for the five Platonic solids.
        INPUT: n(number of vertices), a(alpha; default 0), b(beta; default -1)
        OUTPUT: Huckelsystem instance object (n, a, b)
        """
        matrix = np.zeros((n,n))
        base_cyclic = cls.create_polyene_cyc(n, a, b)
        if n == 4: # tetrahedron
            matrix[1][3]=b
            matrix[0][2]=b

        elif n == 8: # cube
            matrix[0][3]=b
            matrix[1][6]=b
            matrix[2][5]=b
            matrix[4][7]=b

        elif n == 20: # dodecahedron
            matrix[0][4]=b
            matrix[1][11]=b
            matrix[2][9]=b
            matrix[3][7]=b
            matrix[5][18]=b
            matrix[6][16]=b
            matrix[8][15]=b
            matrix[10][14]=b
            matrix[12][19]=b
            matrix[13][17]=b

        elif n == 6: # octahedron
            matrix[0][2]=b
            matrix[0][3]=b
            matrix[1][4]=b
            matrix[1][5]=b
            matrix[2][4]=b
            matrix[3][5]=b

        elif n == 12: # icosahedron
            matrix[0][2]=b
            matrix[0][3]=b
            matrix[0][10]=b
            matrix[1][8]=b
            matrix[1][9]=b
            matrix[1][10]=b
            matrix[2][7]=b
            matrix[2][8]=b
            matrix[3][7]=b
            matrix[3][11]=b
            matrix[4][6]=b
            matrix[4][7]=b
            matrix[4][11]=b
            matrix[5][9]=b
            matrix[5][10]=b
            matrix[5][11]=b
            matrix[6][8]=b
            matrix[6][9]=b

        else:
            print("No Platonic solid of {} vertices.\n\
                create cyclic polyene instead.\n".format(n))
            return base_cyclic

        extra_bridge = matrix + matrix.transpose() # think platonic as
                                                   # cyclic + extra bridges
        platonic = extra_bridge + base_cyclic.matrix

        return cls(platonic, "platonic")

    def degeneracy(self, dp=4):
        """Calculates and prints the degeneracy of the system.
        dp specifies the tolerance in decimal places when comparing
        the energies of the egensystems.
        """
        eigvals = self.evals
        round_eigs = [np.round(item,dp) for item in eigvals]
        round_eigs.sort()
        degen_dict = {}
        for eig1 in round_eigs:
            if eig1 in degen_dict.keys():
                continue
            degen_dict[eig1] = round_eigs.count(eig1)

        #printing part
        print("------------------------------------------------------------------")
        print("eigenvalue(energy) : degeneracy for {} pi system of order {}\n"
            .format(self.type,len(eigvals)))
        for key in degen_dict.keys():
            print("{}  :  {}".format(np.real(key),degen_dict[key]))
        print("------------------------------------------------------------------")
