import numpy as np
import math

""" *Working* file following the handout step-by-step """

H = np.zeros((3,3))
for i in range(3):
    H[i][i]=1
print(H)

eig, eigvec = np.linalg.eig(H)
print(eig)
print(eigvec)

def get_evals(matrix):
    eigvals, eigvecs = np.linalg.eig(matrix)
    return eigvals

#test
#print(get_evals(H))


def create_polyene_lin(n, a=0, b=-1):
    matrix = np.zeros((n,n))
    for i in range(n):
        matrix[i][i] = a
        if i > 0:
            matrix[i][i-1] = b
        if i < n-1:
            matrix[i][i+1] = b
    return matrix

#test
#print(create_polyene_lin(4))
#print(get_evals(create_polyene_lin(4)))
def test(test_fn):
    def test_wrapper(*args, is_print = False, tol=0.0001, **kwargs):
        expected, matrix, n, a, b = test_fn(*args, **kwargs)

        search_count = 0
        if is_print:
            print(expected)
        eigval = get_evals(matrix)
        for i in range(n):
            for j in range(n):
                if math.isclose(eigval[i], expected[j], rel_tol=tol):
                    search_count += 1
                    break

        if search_count == n:
            print("Pass")
        else:
            print("fail")
        return
    return test_wrapper

@test
def test_lin(n, a=0, b=-1):
    matrix = create_polyene_lin(n, a, b)
    expected = [a + 2*b*np.cos(np.pi*(k+1)/(n + 1)) for k in range(n)]
    return expected, matrix, n, a, b


test_lin(4, is_print = True)
test_lin(100)

"======================================"

def create_polyene_cyc(n, a=0, b=-1):
    matrix = np.zeros((n,n))
    for i in range(n):
        matrix[i][i] = a
        matrix[i][i-1] = b
        matrix[i][(i+1)%n] = b
    return matrix

cyc_hex = create_polyene_cyc(6)

print(cyc_hex)
print(get_evals(cyc_hex))

@test
def test_cyc(n, a=0, b=-1):
    matrix = create_polyene_cyc(n)
    expected = [a + 2*b*np.cos(np.pi*2*(k)/n) for k in range(n)]
    return expected, matrix, n, a, b

test_cyc(6, is_print = True)

def degeneracy(eigvals, dp=4):
    round_eigs = [np.round(item,dp) for item in eigvals]
    round_eigs.sort()
    degen_dict = {}
    for eig1 in round_eigs:
        if eig1 in degen_dict.keys():
            continue
        degen_dict[eig1] = round_eigs.count(eig1)

    #printing part
    print("=================================================\nenergy eigenvalue : degeneracy for pi system of order {}\n-------------------------------------------------\n".format(len(eigvals)))
    for key in degen_dict.keys():
        print("{}  :  {}".format(key,degen_dict[key]))



#test get_degeneracy
degeneracy(get_evals(cyc_hex))
degeneracy(get_evals(create_polyene_cyc(10)))

#================================================================
# platonic solids
def create_platonic(n, a=0, b=-1):
    """ Generates Huckel matrix for the five Platonic solids.
    INPUT: n(number of vertices), a(alpha; default 0), b(beta; default -1)
    OUTPUT: numpy ndarray (n,n)
    """
    matrix = np.zeros((n,n))
    if n == 4:
        matrix[1][3]=b
        matrix[0][2]=b

    elif n == 8:
        matrix[0][3]=b
        matrix[1][6]=b
        matrix[2][5]=b

    elif n == 20:
        matrix[0][4]=b
        matrix[1][11]=b
        matrix[2][9]=b
        matrix[3][7]=b
        matrix[5][18]=b
        matrix[6][16]=b
        matrix[8][15]=b
        matrix[10][14]=b
        matrix[12][19]=b
        matrix[13][17]=b

    elif n == 6:
        matrix[0][2]=b
        matrix[0][3]=b
        matrix[1][4]=b
        matrix[1][5]=b
        matrix[2][4]=b
        matrix[3][5]=b

    elif n == 12:
        matrix[0][2]=b
        matrix[0][3]=b
        matrix[0][10]=b
        matrix[1][8]=b
        matrix[1][9]=b
        matrix[1][10]=b
        matrix[2][7]=b
        matrix[2][8]=b
        matrix[3][7]=b
        matrix[3][11]=b
        matrix[4][6]=b
        matrix[4][7]=b
        matrix[4][11]=b
        matrix[5][9]=b
        matrix[5][10]=b
        matrix[5][11]=b
        matrix[6][8]=b
        matrix[6][9]=b

    extra_bridge = matrix + matrix.transpose()
    base_cyclic = create_polyene_cyc(n, a, b)
    platonic = extra_bridge + base_cyclic

    return platonic


tetrahedron = create_platonic(4)
cube = create_platonic(8)
dodecahedron = create_platonic(20)
octahedron = create_platonic(6)
icosahedron = create_platonic(12)

eval_4 = get_evals(tetrahedron)
eval_8 = get_evals(cube)
eval_20 = get_evals(dodecahedron)
eval_6 = get_evals(octahedron)
eval_12 = get_evals(icosahedron)

print("degeneracies of tetrahedron:\n")
degeneracy(eval_4)
print("degeneracies of cube:\n")
degeneracy(eval_8)
print("degeneracies of dodecahedron:\n")
degeneracy(eval_20)
print("degeneracies of octahedron:\n")
degeneracy(eval_6)
print("degeneracies of icosahedron:\n")
degeneracy(eval_12)
