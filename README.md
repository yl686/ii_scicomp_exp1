# II_scicomp_exp1

Huckel Systems

### Files
- exp1.py : a log step-by-step following the handout
- main.py : **the execution file** coded for demonstration of the scripts
- source.py : the soure code containing the class object
- pipfile, pipfile.lock : specifies dependencies for `pipenv`
