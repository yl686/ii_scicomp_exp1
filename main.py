import numpy as np
import math
from source import HuckelSystem

"""
This script is for a nice demonstration of the programme.
"""

#wrapper for testing expected values of tbe eigenvalues
def test(test_fn):
    def test_wrapper(*args, is_print = False, tol=0.0001, **kwargs):
        """Compare the given matrix and the hard-input expected values
           within given tolerance.
           Search the number of corresponding items in the two lists
           and if the number of matching values is equal to the number
           of eivenvalues consider it equal.
        """
        expected, matrix, n, a, b = test_fn(*args, **kwargs)

        search_count = 0
        if is_print:
            print(expected)
        eigval, eigvec = np.linalg.eig(matrix)
        for i in range(n):
            for j in range(n):
                if math.isclose(eigval[i], expected[j], rel_tol=tol):
                    search_count += 1
                    break

        if search_count == n:
            print("Pass")
        else:
            print("fail")
        return
    return test_wrapper


@test
def test_cyc(n, a=0, b=-1):
    """ Create a cyclic Huckel system of n atoms through class instantaiation
    and separately create corresponding Huckel matrix explicitly.
    Compare the two matrices and print 'Pass' or 'Fail' to indicate whether
    the class successfully created a representation of the system.
    """
    cyclic_system = HuckelSystem.create_polyene_cyc(n)
    matrix = cyclic_system.matrix
    expected = [a + 2*b*np.cos(np.pi*2*(k)/n) for k in range(n)]
    return expected, matrix, n, a, b

@test
def test_lin(n, a=0, b=-1):
    """ Create a linear Huckel system of n atoms through class instantaiation
    and separately create corresponding Huckel matrix explicitly.
    Compare the two matrices and print 'Pass' or 'Fail' to indicate whether
    the class successfully created a representation of the system.
    """
    linear_system = HuckelSystem.create_polyene_lin(n, a, b)
    matrix = linear_system.matrix
    expected = [a + 2*b*np.cos(np.pi*(k+1)/(n + 1)) for k in range(n)]
    return expected, matrix, n, a, b

#=================================================
# demonstration
#=================================================
# 1. Test linear and cyclic objects
print("Test linear system with n = 4")
test_lin(4, is_print = True)
print("\nTest linear system with n = 100")
test_lin(100)
print("\nTest cyclic system with n = 6")
test_cyc(6, is_print = True)

# 2. Test get_degeneracy
butadiene = HuckelSystem.create_polyene_lin(4, a=3, b=-2)
cyclohexatriene = HuckelSystem.create_polyene_cyc(6)
print("\n========================================\n")
print("Butadiene with a = 3, b = -2")
print(butadiene)
print("\nOrbital energies of butadiene:")
print(butadiene.evals)
print("\nTest degeneracy of budadiene")
butadiene.degeneracy()
print("\nCyclohexatriene with a = 0, b = -1")
print(cyclohexatriene)
print("\nOrbital energies of cyclohexatriene:")
print(cyclohexatriene.evals)
print("\nTest degeneracy of cyclohexatriene")
cyclohexatriene.degeneracy()

# 3. Create platonic solids
tetrahedron = HuckelSystem.create_platonic(4)
cube = HuckelSystem.create_platonic(8)
dodecahedron = HuckelSystem.create_platonic(20)
octahedron = HuckelSystem.create_platonic(6)
icosahedron = HuckelSystem.create_platonic(12)

# 4. Calculate degeneracies of Platonic polyenes
print("\n==================================================================")
print("Calculate orbital degeneracies of Platonic polyenes...")
print("==================================================================")
print("\n\ndegeneracies of tetrahedron:\n")
tetrahedron.degeneracy()
print("\n\ndegeneracies of cube:\n")
cube.degeneracy()
print("\n\ndegeneracies of dodecahedron:\n")
dodecahedron.degeneracy()
print("\n\ndegeneracies of octahedron:\n")
octahedron.degeneracy()
print("\n\ndegeneracies of icosahedron:\n")
icosahedron.degeneracy()
